var clientServState = function clientServState(){
    this.dependencies = [],
    this.connections = [],
    this.init = function init(initPromise){
        this.sandbox.listen('omnomHttp_newRequest', this.handleNewConnection, this);

        this.start();
        initPromise.resolve();
    };

    this.start = function start(){
        this.sandbox.httpListen(80);
    };

    this.handleNewConnection = function handleNewConnection(conn){
        if(conn.request.url == '/favicon.ico')
        {
            this.sandbox.handleResponse(conn.response, 'serverError', [404, '404 No such file or action found']);
        } else {

            var targetUrl = conn.request.url.split('?')[0];
            targetUrl = targetUrl == '/' ? '/index.html' : targetUrl;
            this.sandbox.handleResponse(conn.response, 'renderWebroot', [targetUrl]);
        }
    }

};

module.exports = clientServState;