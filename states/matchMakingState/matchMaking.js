/**
 * @todo  handle socket disconnects properly such that I don't have orphaned connections.
 */


var PLAYER2FOUND = "player 2 found";
var MATCH_ACCEPTED = "both players accepted match";
var MULTIPLAYER_UNRANKED = 'unrankedGames';
var MULTIPLAYER_PORTLIST = 'portList';

var matchMakingState = function matchMakingState(){

    this.dependencies = ['omnomNet', 'omnomRedis'];

    this.name = 'matchMaking';
    this.primary = {};
    this.subscriber = {}; 

    this.players = [];
    this.waitingPlayers = {};

    this.init = function init(initPromise){
        this.start();
        initPromise.resolve();
    };

    this.start = function start(){
        var that = this;

        this.sandbox.listen('beginMatchMaking', this.initNewContender, this);

        this.primary =  this.sandbox.createRedisClient();
        this.subscriber =  this.sandbox.createRedisClient();

        var portListExistsPromise = that.primary.command('exists', ['portList']);

        //set the list of available 
        portListExistsPromise.then(function(doesExist){
            if(!doesExist){
                for(var x = 2000; x <= 2100; x++){
                    //@todo for the love of god pipeline this / add multi support.
                    that.primary.command('lpush', ['portList', x]);
                }
            }
        });


        this.subscriber.addListener('message', function(channel, message){
            that.handleRedisMessage(channel, message);
        });


    };

    this.initNewContender = function initNewContender(player){
        var that = this;
        player.connection.reset();
        this.players.push(player);

        player.connection.on('cancelSearch', function(){
            that.cancelSearching(player);
        }, this);

        player.connection.on('acceptMatch', function(){
            that.beginGame(player);
        })

        player.connection.on('declineMatch', function(){
            that.declineSearching(player);
        });

        this.findMatch(player);
    }



    this.findMatch = function findMatch(player){
        var that = this;


        var commandPromise = this.primary.command('lpop', [MULTIPLAYER_UNRANKED]);

        commandPromise.then(function(gameId){
            if(!gameId){
                //player 1
                var date = +(new Date());
                var random = Math.floor( (Math.random()*100000000) + 1);
                var gameId = date + '_' + random;

                that.subscriber.command('subscribe', gameId);

                that.primary.command('lpush', [MULTIPLAYER_UNRANKED, gameId]);
                player.gameId = gameId;
                player.playerNumber = 1;

                that.waitingPlayers[gameId] = [player];
            } else {
                player.gameId = gameId;
                player.playerNumber = 2;
                that.waitingPlayers[gameId] = that.waitingPlayers[gameId] || [];
                that.waitingPlayers[gameId].push(player);                    

                that.primary.command('publish', [gameId, PLAYER2FOUND]);
            }
        });

        commandPromise.fail(function(error){
            console.log("Error?", error);
        });
    }

    

    this.handleRedisMessage = function handleRedisMessage(channel, message){
        var messageData = '';
        if(~message.indexOf('##')){
            var messageParts = message.split('##');    
            message = messageParts[0];
            messageData = messageParts[1];
        }
        
        var gameId = channel;
        var players = this.waitingPlayers[gameId];

        switch (message) {
            case PLAYER2FOUND:
                
                //clean this up.
                for(var playerIndex = 0; playerIndex < players.length; playerIndex++){
                    var commandObj =  JSON.stringify({
                        command: 'otherPlayerFound',
                        data: {}
                    });
                    //move this to the socketdatabehavior eventually.
                    players[playerIndex].connection.command('write','##' + commandObj);
                }
                break;
            //this client send should happen in the game state.  this case should send the players into the game state with the game address known,
            // so that the game state can create the simulation process.
            case MATCH_ACCEPTED:
                for(var playerIndex = 0; playerIndex < players.length; playerIndex++){
                    var player = players[playerIndex];
                    this.players.splice(this.players.indexOf(player), 1);
                    this.sandbox.dispatch('enterGameState', player);   
                }
                this.subscriber.command('unsubscribe', [player.gameId]);
                break;
        }
    };

    this.beginGame = function beginGame(player){
        var that = this;
        var gameAddressPromise = this.primary.command('hget', ['activeGames', player.gameId]);

        gameAddressPromise.then(function(gameAddr){
            if(gameAddr){
                player.gameAddress = gameAddr;
                //tell the player the address, ping the game channel to notify other player that the simulation should be started.
                that.primary.command('publish', [player.gameId, MATCH_ACCEPTED + "##" + gameAddr]);
            } else {
                var ipAddress = that.sandbox.getCurrentExternalIP();
                //@todo ADD PIPELINING SUPPORT FFS!  
                var portPromise = that.primary.command('lpop', ['portList']);
                portPromise.then(function(newPort){
                    //@todo add a case for when all ports are taken up?
                    player.gameAddress =  ipAddress + ':' + newPort;
                    that.primary.command('hset', ['activeGames', player.gameId, player.gameAddress]);
                });
                //get current IP address and the topmost port from the portList redis set to create a simulation on that new address.
            }
        });
    };
};

module.exports = matchMakingState;