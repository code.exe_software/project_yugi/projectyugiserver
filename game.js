/**
 * @todo  once I get the tile removal code into the simulation, I can have the server trigger it's own replenish code, which will be AWESOEME.
 */
function bootstrap(){
    var Omnom = require('omnom');

    var omnomOptions = {};


    var pluginDir = './plugins/';
        var plugins = {
        'omnomNet' : 'omnomNet.js',
        'omnomRedis' : 'omnomRedis.js',
        'player': 'playerPlugin.js'
    };

    var omnomPlugins = [];

    for (var name in plugins){
        if(plugins.hasOwnProperty(name)){
            omnomPlugins.push(require(pluginDir + name + "/" + plugins[name]));
        }
    }

    Omnom.init([], omnomPlugins);

    var states = {
        'characterSelect': require('./states/characterSelect/characterSelect.js'),
        'simulation': require('./states/simulationState/simulation.js')
        
    };

    for (var i in states){
        Omnom.register(i, states[i]);
    }
}

bootstrap();

