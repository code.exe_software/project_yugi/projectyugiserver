var READY = 'READY FOR CONNECTION';

var gameState = function gameState(){
    this.dependencies = ['omnomChildProcess'];
    this.name = "game";

    var Player = this.sandbox.Player;
    var mPlayer = new Player();

    this.primary = {};
    this.subscriber = {}; 


    this.simulations = {};

    this.players = {};

    this.init = function init(initPromise){
        this.start();
        initPromise.resolve();
    };

    this.start = function start(){
        var that = this;

        this.sandbox.listen('enterGameState', this.readyPlayer, this);
        this.primary =  this.sandbox.createRedisClient();
        this.subscriber =  this.sandbox.createRedisClient();

        this.subscriber.addListener('message', function(channel, message){
            that.handleRedisMessage(channel, message);
        });
    };

    this.readyPlayer = function readyPlayer(player){
        var that = this;
        player.connection.reset();

        if(!this.players[player.gameId]){
            this.players[player.gameId] = [player];
        } else {
            this.players[player.gameId].push(player);
        }

       

        this.subscriber.command('subscribe', [player.gameId]);

        if(player.playerNumber == 1){
            this.beginSimulation(player);
        }

        player.connection.on('gameOver', function(data){
            player.lostLastGame = data.loser;
            that.exitGame(player);
        });
    };


    this.beginSimulation = function beginSimulation(player){
        var that = this;

        //@todo create simulation, gotta figure out a more graceful way of getting simulation's absolute path.  sigh
        var gameAddr = player.gameAddress;
        var gameAddrComponents = gameAddr.split(':');
        var port = gameAddrComponents[1];


        this.simulations[player.gameId] = this.sandbox.createChildProcess(player.gameId, './game', [port]);

        this.primary.command('publish', [player.gameId, READY]);
    };

    this.handleRedisMessage = function handleRedisMessage(channel, message){
        var messageData = '';
        if(~message.indexOf('##')){
            var messageParts = message.split('##');
            message = messageParts[0];
            messageData = messageParts[1];
        }
        
        var gameId = channel;
        var players = this.players[gameId];

        switch (message) {
            case READY:
                //the simulation is made, tell the players how to connect!
                this.players[channel].forEach(function(pl){
                    pl.connection.command('write', JSON.stringify({
                        command: 'createNewConnection',
                        data: {
                            loc: pl.gameAddress
                        }
                    }));
                });
                break;
        }
    };

    this.exitGame = function exitGame(player){
console.log("EXITING!");
        this.players[player.gameId].splice(this.players[player.gameId].indexOf(player), 1);
console.log("SPLICED");
        /**
         * @todo  once the game id has no more players, clear the game from the this.players object
         * @todo  if the player number is one, return the port to redis.  
         */

        this.sandbox.dispatch('exitGame', player);
    }
    

};

module.exports = gameState;