var Omnom = require('omnom');

module.exports = {
    thisPlayer: {
        events: {
        },
        properties: {
            m_name: 'warden',
            m_masterKeyTileThreshhold: 15,
            m_masterKeyTileWeights : {
                cyan: 0,
                pink: 0,
                green: 0,
                orange: 0,
                blue: 0
            },
            m_lockDownTileThreshhold: 25,
            m_lockDownTileWeights : {
                cyan: 0,
                pink: 0,
                green: 0,
                orange: 0,
                blue: 0
            },
            resolveMatches : function resolveMatches(knownMatches) {
                var i, j;
                var remainingTiles = -1;
                var that = this;
                
                //iterate through the map twice.  once to remove matched tiles, and again to settle the remaining ones into place.
                that.m_gridMap.forEach(function(curColumn, colIndex, map ) {
                    that.m_gridMap[colIndex] = curColumn.filter(function(testTileColor, tileIndex, col) {
                        var result = true;
                        var testCoords = colIndex + ',' + tileIndex;
                        if(~knownMatches.indexOf(testCoords)){
                            var masterKeyIndex = that.m_gridMap[colIndex][tileIndex].indexOf('_masterkey');
                            if(~masterKeyIndex){
                                that.triggerMasterKey();
                            } 

                            var lockDownIndex = that.m_gridMap[colIndex][tileIndex].indexOf('_lockdown');
                            if(~lockDownIndex){
                                that.triggerLockDown();
                            } 

                            var lockedIndex = that.m_lockedMap.indexOf(testCoords);
                            if(~lockedIndex){
                                that.m_lockedMap.splice(lockedIndex, 1);
                                that.m_lockedMap = that.m_lockedMap.map(function(lockedTile){
                                    var lockedCoords = lockedTile.split(',');
                                    if(lockedCoords[0] === colIndex && lockedCoords[1] > tileIndex){
                                        lockedCoords[1]--;
                                    }
                                    return lockedCoords[0] + ',' + lockedCoords[1];
                                });
                            }

                            result = false;
                        }

                        return result;
                    });
                    
                    
                    if (!~remainingTiles || that.m_gridMap[colIndex].length < remainingTiles) {
                        remainingTiles = that.m_gridMap[colIndex].length;
                    }
                });

                this.replenishTiles(knownMatches);

                this.dispatchCommand('updateRemoveTiles', 'self', {tileList: knownMatches, tweenTime: this.m_tweenTime});
                    
                return remainingTiles;
            },
            triggerMasterKey: function triggerMasterKey(){
                //unlock the most recently locked 3 tiles.
                for(var i = 0; i < 3; i++){
                    var lockTileCoords = this.m_lockedMap.pop();
                    if(lockTileCoords){
                        lockTileCoords = lockTileCoords.split(',');
                        this.dispatchCommand('unlockTiles', 'self', {coords: lockTileCoords});
                    }
                }                    
            },
            triggerLockDown: function triggerLockDown(){
                //lock an extra tile on the opponent's board.
                Omnom.omnomEventBus().dispatch('lockTiles', {source: this, numLocked: 3});
                this.dispatchCommand('lockdown', 'self', {});
            },
            replenishTiles: function replenishTiles(matchList){
                var that = this;
                //first figure out which columns need more tiles and how many in each one.
                var colList = [];
                var colDict = {};

                var sortColList = function sortColList(a,b){ return a - b ; };

                for(var matchIndex = 0; matchIndex < matchList.length ; matchIndex++){
                    var coords = matchList[matchIndex].split(',');
                    var coordX = coords[0];
                    if(!~colList.indexOf(coordX)){
                        colList.push(coordX);

                        if(!colDict.hasOwnProperty(coordX)){
                            colDict[coordX] = 0;
                        }

                        colDict[coordX]++;
                    }
                }

                colList.sort(sortColList);

                /**
                 * now for each column we do a replenish.
                 * but we're gonna do some wacky shit to adjust the weights in 
                 * advance to reduce the likelyhood of cascades.
                 */
                var findColorWeightIndex = function findColorWeightIndex(color){
                    var result;
                    for(var i = 0; i < this.m_baseColorWeights.length; i++){
                        if(this.m_baseColorWeights[i].color == color){
                            result = i;
                            break;
                        }
                    }

                    return result;
                };

                var trimWeights = function trimWeights(){
                    var result = true;

                    var evenCol = (col%2 === 0);
                    var sideMatch = evenCol ? row : row-1;

                    if( (col-1) >= 0 && sideMatch >= 0){
                        var matchPrev = that.m_gridMap[col][row-1]; //the block under this one;
                        var matchPrev2 = that.m_gridMap[col-1][sideMatch]; //the first side tile that this could match with.
                        var matchPrev3 = that.m_gridMap[col-1][sideMatch + 1]; // second side tile this could match with

                        /**
                         * @todo add potential matchNext?
                         */
                        if(matchPrev == matchPrev2){
                            that.adjustWeight(matchPrev);
                        } else if ( matchPrev2 == matchPrev3){
                            that.adjustWeight(matchPrev2);
                        }
                    }
                };

                
                for(var colIndex = 0; colIndex < colList.length; colIndex++){
                    var col = +(colList[colIndex]);

                    var boardHeight = (col % 2 === 0) ? 8 : 9;
                    //starting point
                    var startingPoint = this.m_gridMap[colList[colIndex]].length;

                    for( var row = startingPoint; row < boardHeight; row++){
                        trimWeights();
                        var chosenColor = this.generateRandomTile();

                        if(this.m_masterKeyTileWeights[chosenColor] >= this.m_masterKeyTileThreshhold){
                            this.m_masterKeyTileWeights[chosenColor] = 0;
                            this.m_lockDownTileWeights[chosenColor]++;
                            chosenColor += '_masterkey';
                        } else if (this.m_lockDownTileWeights[chosenColor] >= this.m_lockDownTileThreshhold){
                            this.m_lockDownTileWeights[chosenColor] = 0;
                            this.m_masterKeyTileWeights[chosenColor]++
                            chosenColor += '_lockdown';    
                        } else {
                            var whichGetsIncremented = Math.floor(Math.random() * 3 + 1);
                            switch(whichGetsIncremented){
                                case 1: 
                                    this.m_lockDownTileWeights[chosenColor]++;
                                    break;
                                case 2: 
                                    this.m_masterKeyTileWeights[chosenColor]++;
                                    break;
                                case 3:
                                    this.m_lockDownTileWeights[chosenColor]++;
                                    this.m_masterKeyTileWeights[chosenColor]++;
                                    break;
                            }
                                    
                        }

                        this.addTile(chosenColor, col);
                    }
                }
            },
        }
    },
    thatPlayer: {
        
    }
};