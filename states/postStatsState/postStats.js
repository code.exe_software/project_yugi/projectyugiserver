/**
 * @todo  retrieve the game stats from mongo some how to feed to the client because this corresponds to the postgame stats screen
 * on the client.
 */

var postStatsState = function postStatsState(){
    this.players = [];
    this.init = function init(initPromise){
        
        //EventBus handlers
        this.sandbox.listen('exitGame', this.handleNewPlayer, this);
        //this.sandbox.listen('returnToMainMenu', this.handleNewConnection, this);

        initPromise.resolve();
    };


    this.handleNewPlayer = function handleNewPlayer(player){
        var that = this;
        player.connection.reset();
        this.players.push(player);

        //everything is well and good, tell the client to switch states to the main menu.
        var commandObj =  JSON.stringify({
            command: 'stateChange',
            data: {
                statename: "PostGame",
                data: { 
                    loser: player.lostLastGame
                }
            }
        });        
        player.connection.command('write', commandObj);

        player.connection.on('backToMain', function(){
            that.backToMain(player);
        }, this);
    };

    this.backToMain = function backToMain(player){
        this.players.splice(this.players.indexOf(player), 1);

        this.sandbox.dispatch('enterGame', player);
    }


}

module.exports = postStatsState;