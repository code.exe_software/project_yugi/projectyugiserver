var Omnom = require('omnom');

module.exports = {
    thisPlayer: {
        events: {},
        properties: {
            m_name: "spellbinder",
            m_darknessTileThreshhold: 30,
            m_darknessTileWeights : {
                cyan: 0,
                pink: 0,
                green: 0,
                orange: 0,
                blue: 0
            },
            m_lightningstrikeTileThreshhold: 20,
            m_lightningstrikeTileWeights : {
                cyan: 0,
                pink: 0,
                green: 0,
                orange: 0,
                blue: 0
            }, 
            resolveMatches : function resolveMatches(knownMatches) {
                var i, j;
                var remainingTiles = -1;
                var that = this;
                
                //iterate through the map twice.  once to remove matched tiles, and again to settle the remaining ones into place.
                that.m_gridMap.forEach(function(curColumn, colIndex, map ) {
                    var lightningTriggered = false;

                    that.m_gridMap[colIndex] = curColumn.filter(function(testTileColor, tileIndex, col) {
                        var result = true;
                        
                        var testCoords = colIndex + ',' + tileIndex;
                        if(~knownMatches.indexOf(testCoords)){
                            var darknessIndex = that.m_gridMap[colIndex][tileIndex].indexOf('_darkness');
                            if(~darknessIndex){
                                that.triggerDarkness();
                            }

                            var lightningstrikeIndex = that.m_gridMap[colIndex][tileIndex].indexOf('_lightning');
                            if(~lightningstrikeIndex){
                                lightningTriggered = true;
                                that.triggerLightningStrike(colIndex, knownMatches);
                            }

                            var lockedIndex = that.m_lockedMap.indexOf(testCoords);
                            if(~lockedIndex){
                                that.m_lockedMap.splice(lockedIndex, 1);
                                that.m_lockedMap = that.m_lockedMap.map(function(lockedTile){
                                    var lockedCoords = lockedTile.split(',');
                                    if(lockedCoords[0] === colIndex && lockedCoords[1] > tileIndex){
                                        lockedCoords[1]--;
                                    }
                                    return lockedCoords[0] + ',' + lockedCoords[1];
                                });
                            }

                            result = false;
                        }

                        return result;
                    });
                    
                    //hack to remove all tiles affected by lightning assuming it wasn't the first tile in the filter.
                    if(lightningTriggered){
                        that.m_gridMap[colIndex] = [];
                    }
                    
                    
                    if (!~remainingTiles || that.m_gridMap[colIndex].length < remainingTiles) {
                        remainingTiles = that.m_gridMap[colIndex].length;
                    }
                });

                this.replenishTiles(knownMatches);
                this.dispatchCommand('updateRemoveTiles', 'self', {tileList: knownMatches, tweenTime: this.m_tweenTime});
                    
                return remainingTiles;
            },
            triggerDarkness: function triggerDarkness(){
                Omnom.omnomEventBus().dispatch('darknessTriggered', {});
            },
            triggerLightningStrike : function triggerLightningStrike(colIndex, knownMatches){
                for(var i = 0; i < this.m_gridMap[colIndex].length; i++){
                    if(!~knownMatches.indexOf(colIndex + ',' + i)){
                        knownMatches.push(colIndex + ',' + i);
                    }

                    var lockedIndex = this.m_lockedMap.indexOf(colIndex + ',' + i);
                    if(~lockedIndex){
                        this.m_lockedMap.splice(lockedIndex, 1);
                    }
                }
                Omnom.omnomEventBus().dispatch('lockTiles', {source: this, numLocked: 4});
                //particle effects woo!
                this.dispatchCommand('lightningStrike', 'self', {col: colIndex});

            },
            replenishTiles: function replenishTiles(matchList){
                var that = this;
                //first figure out which columns need more tiles and how many in each one.
                var colList = [];
                var colDict = {};

                var sortColList = function sortColList(a,b){ return a - b ; };

                for(var matchIndex = 0; matchIndex < matchList.length ; matchIndex++){
                    var coords = matchList[matchIndex].split(',');
                    var coordX = coords[0];
                    if(!~colList.indexOf(coordX)){
                        colList.push(coordX);

                        if(!colDict.hasOwnProperty(coordX)){
                            colDict[coordX] = 0;
                        }

                        colDict[coordX]++;
                    }
                }

                colList.sort(sortColList);

                /**
                 * now for each column we do a replenish.
                 * but we're gonna do some wacky shit to adjust the weights in 
                 * advance to reduce the likelyhood of cascades.
                 */
                var findColorWeightIndex = function findColorWeightIndex(color){
                    var result;
                    for(var i = 0; i < this.m_baseColorWeights.length; i++){
                        if(this.m_baseColorWeights[i].color == color){
                            result = i;
                            break;
                        }
                    }

                    return result;
                };

                var trimWeights = function trimWeights(){
                    var result = true;

                    var evenCol = (col%2 === 0);
                    var sideMatch = evenCol ? row : row-1;

                    if( (col-1) >= 0 && sideMatch >= 0){
                        var matchPrev = that.m_gridMap[col][row-1]; //the block under this one;
                        var matchPrev2 = that.m_gridMap[col-1][sideMatch]; //the first side tile that this could match with.
                        var matchPrev3 = that.m_gridMap[col-1][sideMatch + 1]; // second side tile this could match with

                        /**
                         * @todo add potential matchNext?
                         */
                        if(matchPrev == matchPrev2){
                            that.adjustWeight(matchPrev);
                        } else if ( matchPrev2 == matchPrev3){
                            that.adjustWeight(matchPrev2);
                        }
                    }
                };


                for(var colIndex = 0; colIndex < colList.length; colIndex++){
                    var col = +(colList[colIndex]);

                    var boardHeight = (col % 2 === 0) ? 8 : 9;
                    //starting point

                    var startingPoint = this.m_gridMap[colList[colIndex]].length;

                    for( var row = startingPoint; row < boardHeight; row++){
                        trimWeights();
                        var chosenColor = this.generateRandomTile();

                        if(this.m_darknessTileWeights[chosenColor] >= this.m_darknessTileThreshhold){
                            this.m_darknessTileWeights[chosenColor] = 0;
                            this.m_lightningstrikeTileWeights[chosenColor]++;
                            chosenColor += '_darkness';
                        } else if (this.m_lightningstrikeTileWeights[chosenColor] >= this.m_lightningstrikeTileThreshhold){
                            this.m_lightningstrikeTileWeights[chosenColor] = 0;
                            this.m_darknessTileWeights[chosenColor]++
                            chosenColor += '_lightning';    
                        } else {
                            var whichGetsIncremented = Math.floor(Math.random() * 3 + 1);
                            switch(whichGetsIncremented){
                                case 1: 
                                    this.m_darknessTileWeights[chosenColor]++;
                                    break;
                                case 2: 
                                    this.m_lightningstrikeTileWeights[chosenColor]++;
                                    break;
                                case 3:
                                    this.m_darknessTileWeights[chosenColor]++;
                                    this.m_lightningstrikeTileWeights[chosenColor]++;
                                    break;
                            }
                                    
                        }

                        this.addTile(chosenColor, col);
                    }
                }
            },
        },
    },
    thatPlayer: {
        events: {
            "darknessTriggered": "handleDarkness"
        },
        properties: {
            handleDarkness: function handleDarkness(){
                this.dispatchCommand('darknessBegin', 'self', {});

                var that = this;

                setTimeout(function(){
                    that.dispatchCommand('darknessEnd', 'self', {});
                }, 10000)
            }
        },
    }
};