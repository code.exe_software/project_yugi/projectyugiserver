var omnom = require('omnom');
var Player = require('./player.js');



module.exports = {
    name: "Player",
    dependencies: [],
    events: [
        'player_connect'
    ],
    omnom: {
        Player: Player
    },
    sandbox: {
        Player : Player
    }
};
