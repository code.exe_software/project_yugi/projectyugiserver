/**
 * @todo  retrieve player data, maybe connect to kongregate API from here?
 */

var mainSplashState = function mainSplashState(){
    this.players = [];
    this.init = function init(initPromise){
        //EventBus handlers
        this.sandbox.listen('omnomNet_newConnection', this.handleNewConnection, this);
        //this.sandbox.listen('returnToMainMenu', this.handleNewConnection, this);

        //OmnomNet connection listener
        this.sandbox.startListening(81);

        //OmnomFlash policy server listener
        this.sandbox.fpsStartListening();

        initPromise.resolve();
    };


    this.handleNewConnection = function handleNewConnection(conn){
        var that = this;

        var newPlayer = {
            id: 12345,
            connection: conn
        }

        this.players.push(newPlayer);

        conn.on('enterGame', function(){
            that.enterGame(newPlayer);
        }, this);
    };

    this.enterGame = function enterGame(player){
        this.players.splice(this.players.indexOf(player), 1);

        this.sandbox.dispatch('enterGame', player);
    }


}

module.exports = mainSplashState;