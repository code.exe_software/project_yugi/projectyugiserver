/**
* @todo add kind of flag that can be checked in settings to see whether the cache-control http header value should be set in such a way as to allow the browser
* to cache the response.  :)
*/

var fs = require('fs');
var util = require('util');
var path = require('path');

// Constructor
var responseHandler = function(res) {
    this.res = res;
};

// properties and methods
responseHandler.prototype = {
    serverError : function(code, content) {
        var headers = {
            'Content-Type': 'text/plain',
            'Content-Length' : content.length
        };
        this.res.writeHead(code, headers);
        this.res.end(content);
    },

    renderHtml : function(content) {
        var headers = {
            'Content-Type': 'text/html',
            'Content-Length' : content.length
        };
        this.res.writeHead(200, headers);
        this.res.end(content, 'utf-8');
    },
    
    renderRedirect: function(target){
        var headers = {
            'Location' : target
        }
        
        this.res.writeHead(302, headers);
        this.res.end()
    },
    
    renderSecuredHtml: function(content, realm)
    {
        realm = realm || "";
        
        var headers = {
            'Content-Type': 'text/html',
            'Content-Length' : content.length,
            'WWW-Authenticate': 'Basic realm="' + realm + '"'
        };

        this.res.writeHead(200, headers);
        this.res.end(content, 'utf-8');
    },
    
    renderAuthenticationFailure: function(realm)
    {
        realm = realm || "";
        var headers = {
            'Content-Type': 'text/html',
            'Content-Length' : 'unauthorized'.length,
            'WWW-Authenticate': 'Basic realm="' + realm + '"'
        };                           
        
        this.res.writeHead(401, headers);
        this.res.end('unauthorized', 'utf-8');
    },

    renderWebroot : function(requestedUrl) {
        //dynamically figure out the proj root based on the entry point.
        var documentRoot = path.dirname(require.main.filename);
        var self = this;
        //try and match a file in our webroot directory
console.log(requestedUrl);
        var extension = (requestedUrl.split('.').pop());

        var filepath = documentRoot + '/webroot' + requestedUrl;
        
        fs.stat(filepath, function(err, stat){
            var headers = {
                'Content-Type' : self.getContentTypeByFileExtension(extension),
                'Content-Length' : stat.size + ''
            };
            
            self.res.writeHead(200, headers);
            
            self.res.on('error', function(err){
            });
            
            var rs = fs.createReadStream(filepath);
            
            rs.on('data', function(data){
                var flushed = self.res.write(data, 'utf-8');
                if(!flushed){
                    rs.pause();   
                }
            });
            
            rs.on('error', function(err){
                console.log(err);
                console.log("STREAM FAILURE");
            });
            
            self.res.on('drain', function(){
               rs.resume(); 
            });
            rs.on('end', function(){
                try{
                    self.res.end();
                }catch (e){
                    console.log("WTF : ");
                    console.log(e.stack);
                    console.log("end wtf");
                }
                
            });   
        });
        
    },
    getContentTypeByFileExtension : function(extension) {
        var header = "";
        
        switch (extension) {
            case 'css':
                header = 'text/css';
                break;
            case 'js':
                header = 'application/javascript';
                break;
            case 'ico':
                header = 'image/x-icon';
                break;
            case 'gif':
            case 'jpg':
            case 'jpeg':
            case 'bmp':
                header = 'image/' + extension;
                break;
            case 'htm':
            case 'html':
                header = 'text/html';
                break;
            case 'swf':
                header = 'application/x-shockwave-flash';
                break;
            default:
                header = 'text/plain';
                break;
        }
        return header;
    }
};
 
// node.js module export
module.exports = responseHandler;