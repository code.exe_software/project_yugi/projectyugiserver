//here we define the data type that will contain the raw connection.
var connection = function connection(socketConn, omnom){
    var customEvents = {};

    this.addListener = function addListener(event, callback){
        if(event == 'data' && socketConn.listeners('data').length > 0){
            console.log("Already an event listener for data.");
            return;
        }
        socketConn.on(event, callback);
    };

    this.removeListener = function removeListener(event, callback){
        socketConn.removeListener(event, callback);
    };

    this.removeAllListeners = function removeAllListeners(event){
        socketConn.removeAllListeners(event);
    }

    /**
     * node redis works off a callback system due to it's async nature.  I'm wrapping this in a promise object
     * that I can manipulate more freely in a similar way  --DM
     */
    this.command = function command(command, argsArray){
        if(!(argsArray instanceof Array)){
            argsArray = [argsArray];
        }

        var netPromise = new omnom.omnomPromise();

        argsArray.push(function(err, result){
            if(err){
                netPromise.reject(err);
            } else {
                netPromise.resolve(result);
            }
        });

        socketConn[command].apply(socketConn, argsArray);

        return netPromise;
    }

    /**
     * kinda reinventing the wheel, but I'd like to think I'm persisting the pattern for event emitters in node.
     * @param string event
     * @param function listener callback
     */
    this.on = function on(event, callback, context){
        if (!customEvents.hasOwnProperty(event)){
            customEvents[event] = [];
        }

        customEvents[event].push({
            callback: callback,
            context: context
        });
    }

    /**
     * I like dispatch over emit.  personal preference --DM
     */
    this.dispatch = function dispatch(event, args){
        if(!customEvents.hasOwnProperty(event)){
            return;
        }

        if(!(args instanceof Array)){
            args = [args];
        }
        
        var len = customEvents[event].length;

        for(var callbackIndex = 0; callbackIndex < len; callbackIndex++){
            customEvents[event][callbackIndex]['callback'].apply(customEvents[event][callbackIndex]['context'], args);
        }
    }

    /**
     * remove first instance of an event listener from an event 
     */
    this.removeEventListener = function removeEventListener(event, cb){
        if(!customEvents.hasOwnProperty(event)){
            return;
        }

        customEvents = customEvents[event].filter(function(el){
            return el['callback'] != cb;
        });
    }

    /**
     * not to be called lightly, this function removes ALL CUSTOM EVENT LISTENERS from the connection object. (note, this does not affect
     * listeners on the SOCKET object.  to do that, you must call <connectionObj>.command('removeAllListeners', <eventToBeReset>))
     */
    this.reset = function reset(){
        customEvents = {};
    }
};

module.exports = connection;