var childProcess = function(cp){
    var that = this;
    //need to send data, and to listen for messages from child.
    /**
     * @todo  add event handlers internally for exit and disconnect.
     */

    this.addListener = function addListener(event, callback){
        if(event == 'message' && cp.listeners('message').length > 0){
            console.log("Already an event listener for message.");
            return;
        }
        cp.on(event, callback);
    };

    this.removeListener = function removeListener(event, callback){
        cp.removeListener(event, callback);
    };

    this.removeAllListeners = function removeAllListeners(event){
        cp.removeAllListeners(event);
    }

    /**
     * not gonna allow sending of anything but json objs
     * @return {[type]} [description]
     */
    this.send = function send(command){
        try{
            JSON.parse(JSON.stringify(command));
        } catch (e) {
            console.log("MALFORMED COMMAND, ONLY JSON ALLOWED");
        }
        
        cp.send(command);
    }

    /**
     * kinda reinventing the wheel, but I'd like to think I'm persisting the pattern for event emitters in node.
     * @param string event
     * @param function listener callback
     */
    this.on = function on(event, callback, context){
        if (!customEvents.hasOwnProperty(event)){
            customEvents[event] = [];
        }

        customEvents[event].push({
            callback: callback,
            context: context
        });
    }

    /**
     * I like dispatch over emit.  personal preference --DM
     */
    this.dispatch = function dispatch(event, args){
        if(!customEvents.hasOwnProperty(event)){
            return;
        }

        var len = customEvents[event].length;

        for(var callbackIndex = 0; callbackIndex < len; callbackIndex++){
            customEvents[event][callbackIndex]['callback'].apply(customEvents[event][callbackIndex]['context'], args);
        }
    }

        /**
     * remove first instance of an event listener from an event 
     */
    this.removeEventListener = function removeEventListener(event, cb){
        if(!customEvents.hasOwnProperty(event)){
            return;
        }

        customEvents = customEvents[event].filter(function(el){
            return el['callback'] != cb;
        });
    }

    /**
     * not to be called lightly, this function removes ALL CUSTOM EVENT LISTENERS from the connection object. (note, this does not affect
     * listeners on the SOCKET object.  to do that, you must call <connectionObj>.command('removeAllListeners', <eventToBeReset>))
     */
    this.reset = function reset(){
        customEvents = {};
    }

}

module.exports = childProcess;