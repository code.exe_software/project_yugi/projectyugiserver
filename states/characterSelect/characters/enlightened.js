var Omnom = require('omnom');

module.exports = {
    thisPlayer: {
        events: {},
        properties: {
            m_name: "karmic",
            m_tweenTime: .75, //slow as hell, needs to come down to virtually nothing as 
            m_canSendLock: false,
            rotate : function rotate(data){
                var validPos = this.validatePosition(this.m_vertexPosition, data.position);

                if(!validPos){
                    //@todo set up an error message here.
                    return false;
                }

                this.m_canSendLock = true;

                data.tweenTime = this.m_tweenTime;
                

                var rotationAttempt = this.resolveRotation(data.direction, this.m_gridMap, this.m_vertexPosition);

                if(rotationAttempt){
                    this.dispatchCommand('updateCursorRotated', 'self', data);
                    //has to be done here instead of in the simulation due to stupid code flow.  --DM
                    var matchList = this.detectMatches(true);

                    while(matchList.knownMatches.length > 0){
                        
                        
                        this.resolveMatches(matchList.knownMatches);
                        //check for cascade to determine if the whileloop should continue.
                        matchList = this.detectMatches();
                    }                    
                }

                return false;
            },
            /**
             * detect matches on blocks surrounding a vertex position.  
             */
            detectMatches : function detectMatches(map) {
                var isFirstDetection = false;
                if (typeof map == 'boolean'){
                    isFirstDetection = map;
                    map = null;
                }

                map = map || this.m_gridMap;
                var matchList = {
                    knownMatches: [], //list of coordinate sets
                    colorsPoints: {} //how many tiles of each color will be removed.
                };
                
                for (var coordX = 0; coordX < 9; coordX++) {
                    for (var coordY = 0; coordY < 15; coordY++) {
                        matchList = this.checkForMatchesAroundVertex(map, coordX + ',' + coordY, matchList);
                    }
                }
                
                return matchList;
            },
            /**
             * rewritten copy of resolveMatches, hopefully like before, bruteforcing will solve a lot of the issues.
             * @param  [String] matchList
             */
            resolveMatches : function resolveMatches(knownMatches) {
                var i, j;
                var remainingTiles = -1;
                var that = this;
                
                //iterate through the map twice.  once to remove matched tiles, and again to settle the remaining ones into place.
                that.m_gridMap.forEach(function(curColumn, colIndex, map ) {
                    that.m_gridMap[colIndex] = curColumn.filter(function(testTileColor, tileIndex, col) {
                        var result = true;
                        var testCoords = colIndex + ',' + tileIndex;
                        if(~knownMatches.indexOf(testCoords)){
                            result = false;
                            var lockedIndex = that.m_lockedMap.indexOf(testCoords);
                            if(~lockedIndex){
                                that.m_lockedMap.splice(lockedIndex, 1);
                                that.m_lockedMap = that.m_lockedMap.map(function(lockedTile){
                                    var lockedCoords = lockedTile.split(',');
                                    if(lockedCoords[0] === colIndex && lockedCoords[1] > tileIndex){
                                        lockedCoords[1]--;
                                    }
                                    return lockedCoords[0] + ',' + lockedCoords[1];
                                });

                                /**
                                 * enlightened only locks enemy tiles if she removes a locked tile of her own.
                                 */
                                Omnom.omnomEventBus().dispatch('lockTiles', {source: that, numLocked: 1});
                                Omnom.omnomEventBus().dispatch('lockTiles', {source: that, numLocked: 1});
                                that.m_canSendLock = false;
                            }
                            //gotta remove the locked tile if it's there.
                            
                        }

                        return result;
                    });
                    
                    
                    if (!~remainingTiles || that.m_gridMap[colIndex].length < remainingTiles) {
                        remainingTiles = that.m_gridMap[colIndex].length;
                    }
                });

                this.replenishTiles(knownMatches);

                this.dispatchCommand('updateRemoveTiles', 'self', {tileList: knownMatches, tweenTime: this.m_tweenTime});
                    
                return remainingTiles;
            },
            lockTiles : function lockTiles(eventData){
                if(eventData.source == this){
                    return;
                }
                var numLocked = eventData.numLocked;
                //var numLocked = 30;

                var lockedTiles = [];

                //locking from the bottom up, so gotta iterate slightly differently
                for (var coordY = 0; coordY < 9 ; coordY++) {
                    for (var coordX = 0; coordX < 10; coordX++) {
                        var coordString = coordX + ',' + coordY;
                        if(!~this.m_lockedMap.indexOf(coordString)){
                            lockedTiles.push(coordString);
                            this.m_lockedMap.push(coordString);
                            numLocked--;
                        }

                        if(numLocked === 0){
                            break;
                        }
                    }

                    if(numLocked === 0){
                        break;
                    }
                }

                var timerReduction = (0.025 * this.m_lockedMap.length);
                timerReduction = timerReduction > 0.50 ? 0.50 : timerReduction;

                this.m_tweenTime = 0.75 - timerReduction;

                this.dispatchCommand('updateLockTiles', 'self', {tileList: lockedTiles});
                this.canStillMove();

                return;
            } 
        },
    },
    thatPlayer: {
        events: {},
        properties: {
        },
    }
};