/**
 * @todo add domains.
 */
var http = require('http');
var responseHandler = require('./responseHandler');
var omnom = require('omnom');

module.exports = {
    name: "omnomHttp",
    dependencies: [],
    events: [
        'omnomHttp_newRequest'
    ],
    omnom: {
        httpListen: function httpListen(port){
            var that = this;

            var httpServer = http.createServer();

            httpServer.listen(port, function(){
                that.omnomLogger('Server running at http://127.0.0.1:' + port + '/'); 
            });
            
            httpServer.on('request', function(request, response){
                that.handleNewRequest(request, response);
            });

            this.httpServer = httpServer;
        }, 
        handleNewRequest: function handleNewRequest(request, response){
            this.dispatch('omnomHttp_newRequest', {
                request: request,
                response: response
            });
        }
    },
    sandbox: {
        httpListen: function httpListen(port){
            /**
             * @todo add validation to the port.
             */
            omnom.httpListen(port);
        },
        handleResponse: function handleResponse(res, funcName, params){
            var responseHandlerObject = new responseHandler(res);

            responseHandlerObject[funcName].apply(responseHandlerObject, params);
        }
    }
}

    