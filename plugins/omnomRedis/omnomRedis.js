/**
 * @todo  handle multi
 * @todo  handle quit
 * @todo  place host/port in config files
 * 
 */

var redis = require('redis');


module.exports = {
    name: "omnomRedis",
    dependencies: [],
    events: [
        'omnomRedis_newConnection',
        'omnomRedis_newSubscription'
    ],
    omnom: {
        redisClients: [],
        createRedisClient: function createRedisClient(host, port){
            var that = this;
            host = host || '127.0.0.1';
            port = port || '6379';

            var redisClient = redis.createClient(port, host);

            var wrappedClient = this.handleNewRedisClient(redisClient);

            this.redisClients.push(wrappedClient); //needed?

            return wrappedClient;
        },
        handleNewRedisClient: function handleNewRedisClient(client){
            var that = this;

            var wrappedClient = function(redisClient){
                this.addListener = function addListener(event, callback){
                    redisClient.on(event, callback);
                };

                /**
                 * node redis works off a callback system due to it's async nature.  I'm wrapping this in a promise object
                 * that I can manipulate more freely.  --DM
                 */
                this.command = function command(command, argsArray){
                    if(!(argsArray instanceof Array)){
                        argsArray = [argsArray];
                    }
                    
                    var redisPromise = new that.omnomPromise();

                    argsArray.push(function(err, result){
                        if(err){
                            redisPromise.reject(err);
                        } else {
                            redisPromise.resolve(result);
                        }
                    });

                    //referencing the command function as a member of the redisClient object so I don't have to use EVAL to call a dynamic function name.  --DM
                    redisClient[command].apply(redisClient, argsArray);

                    return redisPromise;
                }
            };

            return (new wrappedClient(client));
        }
    },
    sandbox: {
        createRedisClient: function createRedisClient(host, port){
            /**
             * @todo add validation to the port.
             */
            return this.omnom.createRedisClient(host, port);
        }
    }
}