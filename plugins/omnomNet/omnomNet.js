var tcp = require('net');
var os = require('os');
var netConnection = require('./netconnection.js');


module.exports = {
	name: "omnomNet",
	dependencies: [],
	events: [
		'omnomNet_newConnection',
		'omnomNet_disconnect',
		'omnomNet_newData'
	],
	omnom: {
		connections: [],
        connsWaitingForMore : [],
        partialCommands : {},
		startListening: function startListening(port){
			var that = this;
			var tcpServer = tcp.createServer();
			
			tcpServer.listen(port);
			
			tcpServer.on('connection', function(socket){
				that.handleNewConnection(socket);
			});

			this.tcpServer = tcpServer;
		},
		/**
		 * eventHandler for new connections from clients.
		 * @param  socket connection
		 */
		handleNewConnection: function handleNewConnection(socket){
			var that = this;
			socket.setNoDelay(true); 


            var newConn = new netConnection(socket, this);

            newConn.addListener('data', function(data){
                that.parseData(data, newConn);
            })

			/**
			 * @todo  handle other relevant events from the socket here.
			 */
			this.dispatch('omnomNet_newConnection', newConn);
		},

        /**
         * handleDAta: this is used as a callback any time data is sent from the client to
         * this socket server.  Since this is a behavior includable in multiple states, it
         * takes measures to only dispatch events within the namespace of the including state.
         * s
         */
        parseData: function handleData(event, connection){
            //I only care about events related to connections in this state.

            var strData = event.toString();

            var commands = strData.split("##");

            var parsedCommands = [];
            
            for (var i in commands){
                //it's possible for split to have the first element be a blank string
                if(!commands[i]){
                    continue;
                }

                var currentCommand = commands[i];

                var waitingConnsIndex = this.connsWaitingForMore.indexOf(connection);

                if(waitingConnsIndex != -1){
                    currentCommand = this.partialCommands[waitingConnsIndex] + currentCommand;
                }

                try {
                    var jsonData = JSON.parse(currentCommand);
                    parsedCommands.push(jsonData);
                    //if parsing didn't throw an error, remove the conn from the waiting list.
                    if(waitingConnsIndex != -1){
                        connsWaitingForMore.splice(waitingConnsIndex, 1);
                    }
                } catch(e) {
                    //if it didn't parse properly, we should assume it's an incomplete command and the rest is coming.
                    if(waitingConnsIndex != -1){
                        this.partialCommands[waitingConnsIndex] = currentCommand;
                    } else {
                        var newIndex = this.connsWaitingForMore.push(connection);
                        this.partialCommands[--newIndex] = currentCommand;
                    }
                }
            }

            for(commIndex = 0; commIndex < parsedCommands.length; commIndex++){
                connection.dispatch(parsedCommands[commIndex]['event'], parsedCommands[commIndex]['data']);
            }


        },
        getCurrentExternalIP: function getCurrentExternalIP(){
            var resultIP = '';
            var interfaces = os.networkInterfaces();
            for(var header in interfaces){
                interfaces[header].forEach(function(details){
                    if(details.family == 'IPv4' && details.internal === false){
                        resultIP = details.address;
                    }
                });
            }

            return resultIP;
        }

	},
	sandbox: {
		startListening: function sandboxStartListening(port){
			/**
			 * @todo add validation to the port.
			 */
			this.omnom.startListening(port);
		},

        getCurrentExternalIP: function getCurrentExternalIP(){
            return this.omnom.getCurrentExternalIP();
        }
	}

}