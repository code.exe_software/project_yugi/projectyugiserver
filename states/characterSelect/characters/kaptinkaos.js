var Omnom = require('omnom');

module.exports = {
    thisPlayer: {
        events: {},
        properties: {
            m_name: (Math.floor(Math.random() * 2)) === 0 ? "Chaos":  "Kaos",
            m_systemShockTileThreshhold: 20,
            m_systemShockTileWeights : {
                cyan: 0,
                pink: 0,
                green: 0,
                orange: 0,
                blue: 0
            },
            lockTiles : function lockTiles(eventData){
                if(eventData.source == this){
                    return;
                }
                var numLocked = eventData.numLocked;
                //var numLocked = 1;

                var lockedTiles = [];

                do {
                    var coordY = Math.floor(Math.random() * 8) + 1;
                    var coordX = Math.floor(Math.random() * 9) + 1;

                    var coordString = coordX + ',' + coordY;

                    if(!~this.m_lockedMap.indexOf(coordString)){
                        lockedTiles.push(coordString);
                        this.m_lockedMap.push(coordString);
                        numLocked--;
                    }
                } while(numLocked > 0);

                this.dispatchCommand('updateLockTiles', 'self', {tileList: lockedTiles});
                this.canStillMove();

                return;
            },
            resolveMatches : function resolveMatches(knownMatches) {
                var i, j;
                var remainingTiles = -1;
                var that = this;
                
                //iterate through the map twice.  once to remove matched tiles, and again to settle the remaining ones into place.
                that.m_gridMap.forEach(function(curColumn, colIndex, map ) {
                    that.m_gridMap[colIndex] = curColumn.filter(function(testTileColor, tileIndex, col) {
                        var result = true;
                        
                        var testCoords = colIndex + ',' + tileIndex;
                        if(~knownMatches.indexOf(testCoords)){
                            var powerIndex = that.m_gridMap[colIndex][tileIndex].indexOf('_systemshock');
                            if(~powerIndex){
                                Omnom.omnomEventBus().dispatch('systemShockTriggered', {});  
                            } 

                            var lockedIndex = that.m_lockedMap.indexOf(testCoords);
                            if(~lockedIndex){
                                that.m_lockedMap.splice(lockedIndex, 1);
                                that.m_lockedMap = that.m_lockedMap.map(function(lockedTile){
                                    var lockedCoords = lockedTile.split(',');
                                    if(lockedCoords[0] === colIndex && lockedCoords[1] > tileIndex){
                                        lockedCoords[1]--;
                                    }
                                    return lockedCoords[0] + ',' + lockedCoords[1];
                                });
                            }

                            result = false;
                        }

                        return result;
                    });
                    
                    
                    if (!~remainingTiles || that.m_gridMap[colIndex].length < remainingTiles) {
                        remainingTiles = that.m_gridMap[colIndex].length;
                    }
                });

                this.replenishTiles(knownMatches);

                this.dispatchCommand('updateRemoveTiles', 'self', {tileList: knownMatches, tweenTime: this.m_tweenTime});
                    
                return remainingTiles;
            },
            /**
             * detect matches on blocks surrounding a vertex position.  
             */
            detectMatches : function detectMatches(map) {
                var isFirstDetection = false;
                if (typeof map == 'boolean'){
                    isFirstDetection = map;
                    map = null;
                }

                map = map || this.m_gridMap;
                var matchList = {
                    knownMatches: [], //list of coordinate sets
                    colorsPoints: {} //how many tiles of each color will be removed.
                };
                
                for (var coordX = 0; coordX < 9; coordX++) {
                    for (var coordY = 0; coordY < 15; coordY++) {
                        matchList = this.checkForMatchesAroundVertex(map, coordX + ',' + coordY, matchList);
                    }
                }

                if(matchList.knownMatches.length && map == this.m_gridMap && isFirstDetection){
                    Omnom.omnomEventBus().dispatch('lockRandom', {source: this, numLocked: 1});
                }
                
                return matchList;
            },
            replenishTiles: function replenishTiles(matchList){
                var that = this;
                //first figure out which columns need more tiles and how many in each one.
                var colList = [];
                var colDict = {};

                var sortColList = function sortColList(a,b){ return a - b ; };

                for(var matchIndex = 0; matchIndex < matchList.length ; matchIndex++){
                    var coords = matchList[matchIndex].split(',');
                    var coordX = coords[0];
                    if(!~colList.indexOf(coordX)){
                        colList.push(coordX);

                        if(!colDict.hasOwnProperty(coordX)){
                            colDict[coordX] = 0;
                        }

                        colDict[coordX]++;
                    }
                }

                colList.sort(sortColList);

                /**
                 * now for each column we do a replenish.
                 * but we're gonna do some wacky shit to adjust the weights in 
                 * advance to reduce the likelyhood of cascades.
                 */
                var findColorWeightIndex = function findColorWeightIndex(color){
                    var result;
                    for(var i = 0; i < this.m_baseColorWeights.length; i++){
                        if(this.m_baseColorWeights[i].color == color){
                            result = i;
                            break;
                        }
                    }

                    return result;
                };

                var trimWeights = function trimWeights(){
                    var result = true;

                    var evenCol = (col%2 === 0);
                    var sideMatch = evenCol ? row : row-1;

                    if( (col-1) >= 0 && sideMatch >= 0){
                        var matchPrev = that.m_gridMap[col][row-1]; //the block under this one;
                        var matchPrev2 = that.m_gridMap[col-1][sideMatch]; //the first side tile that this could match with.
                        var matchPrev3 = that.m_gridMap[col-1][sideMatch + 1]; // second side tile this could match with

                        /**
                         * @todo add potential matchNext?
                         */
                        if(matchPrev == matchPrev2){
                            that.adjustWeight(matchPrev);
                        } else if ( matchPrev2 == matchPrev3){
                            that.adjustWeight(matchPrev2);
                        }
                    }
                };

                
                for(var colIndex = 0; colIndex < colList.length; colIndex++){
                    var col = +(colList[colIndex]);

                    var boardHeight = (col % 2 === 0) ? 8 : 9;
                    //starting point
                    var startingPoint = this.m_gridMap[colList[colIndex]].length;

                    for( var row = startingPoint; row < boardHeight; row++){
                        trimWeights();
                        var chosenColor = this.generateRandomTile();

                        if(this.m_systemShockTileWeights[chosenColor] >= this.m_systemShockTileThreshhold){
                            for(var i in this.m_systemShockTileWeights){
                                this.m_systemShockTileWeights[i] = 0;    
                            }
                            

                            chosenColor += '_systemshock';
                        } else {
                            that.m_systemShockTileWeights[chosenColor]++;
                        }

                        this.addTile(chosenColor, col);
                    }
                }
            }
        },
    },
    thatPlayer: {
        events: {
            "systemShockTriggered": "handleSystemShock",
            "lockRandom": "lockRandom"
        },
        properties: {
            lockRandom : function lockRandom(eventData){
                if(eventData.source == this){
                    return;
                }
                var numLocked = eventData.numLocked;
                //var numLocked = 1;

                var lockedTiles = [];

                do {
                    var coordY = Math.floor(Math.random() * 8) + 1;
                    var coordX = Math.floor(Math.random() * 9) + 1;

                    var coordString = coordX + ',' + coordY;

                    if(!~this.m_lockedMap.indexOf(coordString)){
                        lockedTiles.push(coordString);
                        this.m_lockedMap.push(coordString);
                        numLocked--;
                    }
                } while(numLocked > 0);

                this.dispatchCommand('updateLockTiles', 'self', {tileList: lockedTiles});
                this.canStillMove();

                return;
            },
            handleSystemShock: function handleSystemShock(){

                var holderFunction = this.rotate;
                var that = this;

                this.rotate = this.randomRotate;
                setTimeout(function(){
                    that.rotate = holderFunction;
                }, 10000);
            },
            randomRotate : function randomRotate(data){
                data.tweenTime = this.m_tweenTime;
                
                var randomVertexPosition = [Math.floor(Math.random() * 9) + 1, Math.floor(Math.random() * 8) + 1];

                var rotationAttempt = this.resolveRotation(data.direction, this.m_gridMap, randomVertexPosition);

                if(rotationAttempt){
                    data.position = randomVertexPosition;
                    this.dispatchCommand('updateCursorRotated', 'self', data);  
                } 

                return rotationAttempt;
            },
        },
    }
};