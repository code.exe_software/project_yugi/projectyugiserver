var cp = require('child_process');
var childProcess = require('./childProcess.js');

module.exports = {
    name: "omnomChildProcess",
    dependencies: [],
    omnom: {
        childProcesses: {},
        /**
         * create a new child process in node that can be spoken to by the parent.
         * @param  string id   local identifier, not OS pid.
         * @param  array args command line args to give the new process.
         * @return void
         */
        createChildProcess: function createChildProcess(id, path, args, opts){
            /**
             * @todo  add error state when you have two processes with one id?
             */
            args = args || [];
            opts = opts || {};
            if(!this.childProcesses[id]){
                var that = this;
                var newCp = cp.fork(path, args, opts);
                this.childProcesses[id] = new childProcess(newCp);

                //not currently supporting transmitting tcp or http servers from process process yet.
                this.childProcesses[id].addListener('message', function(data, serverObj){
                    that.handleMessage(data,  that.childProcesses[id]);
                });
            }

            return this.childProcesses[id];
        },
        /**
         * handle data being received from a child process.
         * @param  json event some form of eventObject
         * @param  child_process cp    process created via child_process.fork()
         */
        handleMessage: function handleMessage(event, cp){
            //I only care about events related to connections in this state.

            cp.dispatch(event['event'], event['data']);
        }
    },
    sandbox: {
        createChildProcess : function createChildProcess(newId, path, args, opts){
            return this.omnom.createChildProcess(newId, path, args, opts);
        }
    }
};