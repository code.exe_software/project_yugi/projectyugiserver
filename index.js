var os = require('os');
var cluster = require('cluster');

var env = process.env.NODE_ENV;



switch (env){
	case 'production':
	case 'staging':
		clusterBootstrap();
		break;
	case 'dev':
	default:
		bootstrap();
		break;
}

function clusterBootstrap(){
	if(cluster.isMaster){
		for(var i = 0;i<os.cpus().length; i++){
			console.log('Booting up worker');
			cluster.fork();
		}

		//cluster.on('disconnect', reviveWorker);
	}
	else{
		bootStrap();
	}
}

	

function bootstrap(){
	var Omnom = require('omnom');

	var omnomOptions = {};


    var pluginDir = './plugins/';
	var plugins = {
		'omnomNet' : 'omnomNet.js',
		'omnomChildProcess': 'omnomChildProcess.js',
		'omnomFlash' : 'omnomFlash.js',
		'omnomHttp' : 'omnomHttp.js',
		'omnomRedis' : 'omnomRedis.js',
        'player' : 'playerPlugin.js'
    };

	var omnomPlugins = [];

	for (var name in plugins){
        if(plugins.hasOwnProperty(name)){
		    omnomPlugins.push(require(pluginDir + name + "/" + plugins[name]));
        }
	}

	Omnom.init(omnomOptions, omnomPlugins);

	var states = {
		'mainSplash': require('./states/mainSplashState/mainSplash.js'),
		'mainMenu': require('./states/mainMenuState/mainMenu.js'),
		'modeSelect': require('./states/modeSelectState/modeSelect.js'),
		'matchMaking': require('./states/matchMakingState/matchMaking.js'),
		'game': require('./states/gameState/game.js'),
		'postStats': require('./states/postStatsState/postStats.js'),
		'clientServ': require('./states/clientServState/clientServ.js')
	}

	for(i in states){
		Omnom.register(i, states[i]);
	}
}